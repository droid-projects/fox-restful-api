package br.com.ldrson.fox;


import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class SimpleHttpClient {

    private String AUTENTICACAO_USUARIO;
    private String AUTENTICACAO_SENHA;


    // connection timeout, in milliseconds (waiting to connect)
    //private static final int CONN_TIMEOUT = 10000;

    // socket timeout, in milliseconds (waiting for data)
    //private static final int SOCKET_TIMEOUT = 10000;

    private RequestType mRequestType;

    //private String processMessage = "Processing...";


    private List<NameValuePair> params = new ArrayList<NameValuePair>();
    private List<NameValuePair> headers = new ArrayList<NameValuePair>();

    public SimpleHttpClient(RequestType requestType){
        mRequestType = requestType;
    }

    public void setUserAuthentication(String user, String password){
        AUTENTICACAO_USUARIO = user;
        AUTENTICACAO_SENHA = password;
    }

    public void addNameValuePairs(List<br.com.ldrson.fox.NameValuePair> parameters) {

        for(br.com.ldrson.fox.NameValuePair pair : parameters) {
            params.add(new BasicNameValuePair(pair.getName(), pair.getValue()));
        }
    }


    public void addHeaders(List<br.com.ldrson.fox.NameValuePair> parameters) {

        for(br.com.ldrson.fox.NameValuePair pair : parameters) {
            headers.add(new BasicNameValuePair(pair.getName(), pair.getValue()));
        }
    }

    public String execute(String url) {

        String result = "";

        HttpResponse response = doResponse(url);

        if (response == null) {
            return result;
        } else {

            try {

                Header contentEncoding = response.getFirstHeader("Content-Encoding");
                if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {

                    InputStream instream = response.getEntity().getContent();
                    result = inputStreamToString( new GZIPInputStream(instream));
                }else {
                    result = inputStreamToString(response.getEntity().getContent());
                }

            } catch (Exception e) {
                System.out.println("Erro : "+e.getMessage());
            }

        }

        return result;
    }

    // Establish connection and socket (data retrieval) timeouts
    private HttpParams getHttpParams() {

        HttpParams htpp = new BasicHttpParams();

        //HttpConnectionParams.setConnectionTimeout(htpp, CONN_TIMEOUT);
        //HttpConnectionParams.setSoTimeout(htpp, SOCKET_TIMEOUT);


        return htpp;
    }

    private HttpResponse doResponse(String url) {

        HttpClient httpclient = null;

        if(url.contains("https")){

            HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

            DefaultHttpClient client = new DefaultHttpClient();

            SchemeRegistry registry = new SchemeRegistry();
            SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
            socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
            registry.register(new Scheme("https", socketFactory, 443));
            SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);

            httpclient = new DefaultHttpClient(mgr, getHttpParams());

            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

        }else{

            httpclient = new DefaultHttpClient(getHttpParams());

        }


        HttpResponse response = null;

        try {
            switch (mRequestType) {

                case POST:
                    HttpPost httppost = new HttpPost(url);
                    // Add parameters application/mime
                    //Base64 b = new Base64();
                    //String encoding = b.encode("becomexapi:n3wpwd@b3c0m3x");
                    httppost.addHeader( BasicScheme.authenticate(new UsernamePasswordCredentials(AUTENTICACAO_USUARIO, AUTENTICACAO_SENHA), "UTF-8", false));

                    httppost.addHeader("Accept-Encoding", "gzip");

                    for(NameValuePair pair : headers){
                        httppost.addHeader(pair.getName(),pair.getValue());
                    }


                    httppost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

                    response = httpclient.execute(httppost);
                    break;
                case GET:
                    HttpGet httpget = new HttpGet(url);

                    httpget.addHeader( BasicScheme.authenticate(new UsernamePasswordCredentials(AUTENTICACAO_USUARIO, AUTENTICACAO_SENHA), "UTF-8", false));
                    httpget.addHeader("Accept-Encoding", "gzip");

                    for(NameValuePair pair : headers){
                        httpget.addHeader(pair.getName(),pair.getValue());
                    }

                    response = httpclient.execute(httpget);
                    break;
            }
        } catch (Exception e) {

            System.out.print("Erro "+e.getMessage());

        }

        return response;
    }

    private String inputStreamToString(InputStream is) {

        String line;
        StringBuilder total = new StringBuilder();

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            // Read response until the end
            while ((line = rd.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
           System.out.print("Erro "+e.getMessage());
        }

        // Return full string
        return total.toString();
    }


}
