package br.com.ldrson.fox;

/**
 * Created by leanderson on 14/06/16.
 */
public enum RequestType {

    GET(1), POST(2), DELETE(3);

    private int value;

    private RequestType(int value){
        this.value = value;
    }

    public int getValue(){
        return value;
    }

}
