package br.com.ldrson.fox;

/**
 * Created by leanderson on 11/03/16.
 */
public interface Request {

    /**
     * Executar a requisição HTTP
     */
    String execute() throws Exception;

}
