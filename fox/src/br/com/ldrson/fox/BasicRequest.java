package br.com.ldrson.fox;

import java.util.List;

/**
 * Created by leanderson on 14/06/16.
 */
public class BasicRequest implements Request{

    private String mRequestUrl;
    private  RequestType mRequestType;
    private List<NameValuePair> mHeaders;
    private List<NameValuePair> mParameters;
    private NameValuePair mBasicAuthorization;


    public BasicRequest(String requestUrl
                      , RequestType requestType
                      , List<NameValuePair> headers
                      , List<NameValuePair> parameters
                      , NameValuePair basicAuthorization){

        mRequestUrl = requestUrl;
        mRequestType = requestType;
        mHeaders = headers;
        mParameters = parameters;
        mBasicAuthorization = basicAuthorization;

    }


    @Override
    public String execute() throws Exception {

        SimpleHttpClient httpClient = new SimpleHttpClient(mRequestType);

        if(mBasicAuthorization != null){
            httpClient.setUserAuthentication(mBasicAuthorization.getName(),mBasicAuthorization.getValue());
        }

        httpClient.addNameValuePairs(mParameters);
        httpClient.addHeaders(mHeaders);

        return httpClient.execute(mRequestUrl);
    }


}
