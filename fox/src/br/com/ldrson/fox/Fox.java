package br.com.ldrson.fox;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leanderson on 14/06/16.
 */
public class Fox {


    public static Builder with(String requestUrl){
        return new Builder(requestUrl);
    }

    public static final class Builder{

        private String mRequestUrl;
        private RequestType mRequestType;
        private List<NameValuePair> mHeaders;
        private List<NameValuePair> mParameters;
        private NameValuePair mBasicAuthorization;

        public Builder(String requestUrl){
            mRequestUrl = requestUrl;
            mHeaders = new ArrayList<>();
            mParameters = new ArrayList<>();
        }

        public Builder andRequestType(RequestType type){
            mRequestType = type;
            return this;
        }

        public Builder andRequestPost(){
            mRequestType = RequestType.POST;
            return this;
        }

        public Builder andRequestGet(){
            mRequestType = RequestType.GET;
            return this;
        }

        public Builder andRequestDelete(){
            mRequestType = RequestType.DELETE;
            return this;
        }

        public Builder andHeader(String name, String value){
            mHeaders.add(new NameValuePair(name, value));
            return this;
        }

        public Builder andBasicAuthorization(String user, String password){
            mBasicAuthorization = new NameValuePair(user,password);
            return this;
        }

        public Builder andParameter(String name, String value){
            mParameters.add(new NameValuePair(name, value));
            return this;
        }

        public Request build() throws Exception {
            if(mRequestType == null){
                throw new Exception("Tipo da Requisição HTTP não foi informado. ");
            }
            Request request = new BasicRequest(mRequestUrl,mRequestType,mHeaders,mParameters,mBasicAuthorization);
            return request;
        }

        public String buildAndExecute() throws Exception{
            return build().execute();
        }

    }


}
