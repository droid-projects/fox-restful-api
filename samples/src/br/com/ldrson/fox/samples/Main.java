package br.com.ldrson.fox.samples;

import br.com.ldrson.fox.Fox;

/**
 * Created by leanderson on 14/06/16.
 */
public class Main {

    public static void main(String args[]){
        try {

            String result = Fox.with("http://localhost:8080/usuario/validar_acesso_do_dispositivo")
                    .andRequestPost()
                    .andHeader("HeaderName", "valueOfHeader")
                    .andBasicAuthorization("DESENVOLVEDOR", "TESTE12345")
                    .andParameter("email", "007@mi6.com")
                    .andParameter("senha","ultrasecreto")
                    .andParameter("token","5345345sdfsadfaq45sdafasdf")
                    .andParameter("sistemaOperacional","ANDROID").buildAndExecute();

            System.out.println(result);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
